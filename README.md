Role Name
=========

Deploys container with Victoria Metrics

Requirements
------------

No special requirements

Role Variables
--------------
victoria_container_name - name of conatiner, mandatory
victoria_docker_image - docker image, default is "victoriametrics/victoria-metrics:v1.28.0"
victoria_docker_networks - list of networks, default is omit
victoria_docker_commands - list of commands to run on start, default is omit
victoria_docker_ports - list of port mappings to host network, default is omit
victoria_exposed_ports - list of exposed ports, default is omit
victoria_data_dir - name of volume or direstory to bind as a data directory
victoria_docker_volumes - list of volumes/bindings, default is "- "{{ victoria_data_dir }}:/victoria-metrics-data""
victoria_docker_restart_policy - restart policy for container, default is "always"
victoria_docker_restart - set yes to force container to be restarted, default is no
victoria_docker_state - desired state of container, default is "started"
victoria_docker_env - dict with environment variables, default is omit
victoria_docker_labels - dict with container labels, default is omit
victoria_auto_remove - set to auto remove container when container's procces exits, default is omit
victoria_network_mode - docker network mode, default is omit
victoria_detach_mode - set to no, if you do not want to start container in detached mode, default is omit





Dependencies
------------

No depedencies

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

Victor Kurianov 
